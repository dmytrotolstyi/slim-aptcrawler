<?php
/**
 * Created by PhpStorm.
 * User: Angel
 * Date: 11.08.2016
 * Time: 16:00
 */
namespace App\Middlewares;
class ApiMiddleware {

    public function __construct($root) {
        $this->root = $root;
    }

    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next) {
        if (preg_match('|^' . $this->root . '.*|', $request->getUri()->getPath())) {

            // Force response headers to JSON
            $response = $response->withHeader(
                'Content-Type',
                'application/json'
            );

            $method = strtolower($request->getMethod());
            $mediaType = $request->getMediaType();

            if (in_array(
                $method,
                array('post', 'put', 'patch')
            )) {

                if (empty($mediaType)
                    || $mediaType !== 'application/json') {
                    $response = $response->withStatus(415);
                }
            }
        }

        $response = $next($request, $response);
        return $response;
    }

}