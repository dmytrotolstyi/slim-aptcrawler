<?php
/**
 * Created by PhpStorm.
 * User: Angel
 * Date: 12.08.2016
 * Time: 13:26
 */

namespace App\Middlewares;


class PaginationMiddleware {

    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next) {
        $paginationParams = \App\Utils\Pagination::getParams($request);
        $request = $request->withAttribute('pagination', $paginationParams);

        $response = $next($request, $response);
        return $response;
    }
}