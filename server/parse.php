<?php
error_reporting( E_ALL );
ini_set('display_errors', 1);
/**
 * Created by PhpStorm.
 * User: Angel
 * Date: 03.08.2016
 * Time: 17:33
 */

$string = file_get_contents("../backup/db.json");
$json = json_decode($string, true);
$result = '';
for ($i = 0; $i < count($json); $i++) {
    $result .= "INSERT INTO `apartments` (`id`, `price`, `bedrooms`, `space`, `interior`, `details`) VALUES (NULL, " .
        $json[$i]['price'] . "," .
        $json[$i]['bedrooms'] . "," .
        (empty($json[$i]['space']) ? 'NULL' : $json[$i]['space']) . "," .
        "'" . $json[$i]['interior'] . "',";
    unset($json[$i]['price']);
    unset($json[$i]['bedrooms']);
    unset($json[$i]['space']);
    unset($json[$i]['interior']);
    $details = json_encode($json[$i]);
    $result .= "'" . str_replace("'", "''", $details) . "');<br>";
}
print_r($result);