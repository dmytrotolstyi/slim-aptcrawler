<?php
class Connection
{
    protected $db;

    public function Connection($host, $dbname, $username, $password)
    {
        $conn = NULL;
        try{
            $connString = sprintf("mysql:host=%s;dbname=%s;charset=utf8;collation=utf8_unicode_ci", $host, $dbname);
            $conn = new PDO($connString, $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
        $this->db = $conn;
    }

    public function getConnection()
    {
        return $this->db;
    }
}