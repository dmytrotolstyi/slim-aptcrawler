<?php
/**
 * Created by PhpStorm.
 * User: Angel
 * Date: 05.08.2016
 * Time: 17:47
 */
namespace App\Models;
class Apartment {
    public $id;
    public $title;
    public $price;
    public $bedrooms;
    public $space;
    public $url;
    public $type;
    public $description;
    public $postalCode;
    public $interior;
    public $agency;
    public $street;
    public $district;
    public $priceInclusive;
    public $availableFrom;

    public function __construct($dbApartment) {
        $fields = ['id', 'price', 'bedrooms', 'space', 'interior'];
        $this->extractValues($fields, $dbApartment);
        $this->parseDetails(json_decode($dbApartment['details']));
    }

    private function parseDetails($details) {
        $fields = array('title', 'url', 'type', 'description', 'postalCode', 'interior', 'agency', 'street', 'district', 'priceInclusive', 'availableFrom');
        $this->extractValues($fields, (array)$details);
    }

    /**
     * Extracts specified fields from array and assigns to current object
     * @param $fields
     * @param $array
     */
    private function extractValues($fields, $array) {
        foreach($fields as $field) {
            if (array_key_exists($field, $array)) {
                $this->{$field} = $array[$field];
            }
        }

    }
}