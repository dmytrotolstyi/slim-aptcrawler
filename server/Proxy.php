<?php
/**
 * Created by PhpStorm.
 * User: Angel
 * Date: 04.08.2016
 * Time: 14:07
 */

require_once 'connection.php';

class Proxy
{
    private $conn = NULL;
    private $config = NULL;

    public function __construct($config) {
        $this->config = $config;
    }
    
    # getConnection
    public function getConnection()
    {
        if (is_null($this->conn)) {
            $this->conn = new Connection(
                $this->config['host'],
                $this->config['dbname'],
                $this->config['username'],
                $this->config['password']);
        }
        return $this->conn->getConnection();
    }
}