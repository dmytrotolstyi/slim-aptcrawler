<?php

require 'vendor/autoload.php';
require_once 'Database.php';

$config = include('./config.php');

$app = new Slim\App(array(
    'appConfig' => $config,
    'debug' => true
));

$container = $app->getContainer();

$container['view'] = new \Slim\Views\PhpRenderer("templates/");

$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('AppLoger');
    $file_handler = new \Monolog\Handler\StreamHandler("../logs/app.log");
    $logger->pushHandler($file_handler);
    return $logger;
};

$container['db'] = function($c) {
    $db = new Database($c->appConfig['db']);
    return $db;
};

$app->get('/', function ($request, $response, $args) {
    $apartments = $this->db->getApartments($request);
    $response = $this->view->render($response, "index.phtml", ['apartments' => $apartments]);
});

//$app->notFound(function () use ($app) {
//
//    $mediaType = $app->request->getMediaType();
//
//    $isAPI = (bool) preg_match('|^/api/v.*$|', $app->request->getPath());
//
//
//    if ('application/json' === $mediaType || true === $isAPI) {
//
//        $app->response->headers->set(
//            'Content-Type',
//            'application/json'
//        );
//
//        echo json_encode(
//            array(
//                'code' => 404,
//                'message' => 'Not found'
//            )
//        );
//
//    } else {
//        echo '<html>
//        <head><title>404 Page Not Found</title></head>
//        <body><h1>404 Page Not Found</h1><p>The page you are
//        looking for could not be found.</p></body></html>';
//    }
//});

//$app->error(function (\Exception $e) use ($app) {
//
//    $mediaType = $app->request->getMediaType();
//
//    $isAPI = (bool) preg_match('|^/api/v.*$|', $app->request->getPath());
//
//    // Standard exception data
//    $error = array(
//        'code' => $e->getCode(),
//        'message' => $e->getMessage(),
//        'file' => $e->getFile(),
//        'line' => $e->getLine(),
//    );
//
//    // Graceful error data for production mode
//    if (!in_array(
//            get_class($e),
//            array('API\\Exception', 'API\\Exception\ValidationException')
//        )
//        && 'production' === $app->config('mode')) {
//        $error['message'] = 'There was an internal error';
//        unset($error['file'], $error['line']);
//    }
//
//    // Custom error data (e.g. Validations)
//    if (method_exists($e, 'getData')) {
//        $errors = $e->getData();
//    }
//
//    if (!empty($errors)) {
//        $error['errors'] = $errors;
//    }
//
//    $log->error($e->getMessage());
//    if ('application/json' === $mediaType || true === $isAPI) {
//        $app->response->headers->set(
//            'Content-Type',
//            'application/json'
//        );
//        echo json_encode($error);
//    } else {
//        echo '<html>
//        <head><title>Error</title></head>
//        <body><h1>Error: ' . $error['code'] . '</h1><p>'
//            . $error['message']
//            .'</p></body></html>';
//    }
//
//});

require './api.php';

// Middlewares
$app->add(new \App\Middlewares\PaginationMiddleware);

$app->run();