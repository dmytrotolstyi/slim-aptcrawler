<?php
/**
 * Created by PhpStorm.
 * User: Angel
 * Date: 11.08.2016
 * Time: 13:14
 */

// API group
$app->group('/api', function () use ($app) {

    // Version group
    $app->group('/v1', function () use ($app) {

        $app->get('/apts', function($request, $response, $args) {
            $this->logger->addInfo('Request: ', array($request));
            $apartments = $this->db->getApartments($request);
            return $response->withJson($apartments);
        });

        $app->get('/apts/{id}', function ($request, $response, $args) {
            return $response->withJson('Not implemented', 404);
        });
    });
});