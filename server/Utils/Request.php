<?php

/**
 * Created by PhpStorm.
 * User: Angel
 * Date: 11.08.2016
 * Time: 18:50
 */
namespace App\Utils;

class Request {
    static function getParam($request, $paramName, $defaultValue) {
        $params = $request->getQueryParams();
        if (isset($params[$paramName])) {
            return $params[$paramName];
        } else {
            return $defaultValue;
        }
    }
}