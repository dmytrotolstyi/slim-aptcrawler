<?php
/**
 * Created by PhpStorm.
 * User: Angel
 * Date: 11.08.2016
 * Time: 18:56
 */

namespace App\Utils;

class Pagination
{
    static function getParams($request) {
        $page = \App\Utils\Request::getParam($request, 'page', 1);
        $page = (int)filter_var($page, FILTER_SANITIZE_NUMBER_INT);

        $count = \App\Utils\Request::getParam($request, 'count', 10);
        $count = (int)filter_var($count, FILTER_SANITIZE_NUMBER_INT);

        $limit = $count;
        $offset = ($page - 1) * $count;

        return array(
            'limit' => $limit,
            'offset' => $offset
        );
    }
}