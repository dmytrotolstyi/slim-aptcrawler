<?php

require_once 'Proxy.php';

/**
 * Created by PhpStorm.
 * User: Angel
 * Date: 11.08.2016
 * Time: 18:15
 */
class Database {
    public function __construct($config) {
        $proxy = new Proxy($config);
        $this->connection = $proxy->getConnection();
    }

    /**
     * Retrieve apartments entries from database
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return array
     */
    public function getApartments($request) {
        $paginationParams = $request->getAttribute('pagination');

        $stmt = $this->connection->prepare(
            'SELECT * FROM apartments LIMIT :limit OFFSET :offset',
            array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));

        $stmt->bindParam(':limit', $paginationParams['limit'], PDO::PARAM_INT);
        $stmt->bindParam(':offset', $paginationParams['offset'], PDO::PARAM_INT);
        $stmt->execute();
        $dbApartments = $stmt->fetchAll();

        $apartments = array();
        foreach($dbApartments as $dbApartment) {
            $apartments[] = new App\Models\Apartment($dbApartment);
        }

        return $apartments;
    }
}