var ExtractTextPlugin = require("extract-text-webpack-plugin");
var LiveReloadPlugin = require('webpack-livereload-plugin');
var webpack = require('webpack');

module.exports = {
  entry: {
    vendor: ['jquery'],
    app: './src/index.js'
  },
  output: {
    path: 'server',
    filename: 'scripts/[name].bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components|assets)/,
        loader: 'babel', // 'babel-loader' is also a legal name to reference
        query: {
          presets: ['es2015']
        }
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        loader: ExtractTextPlugin.extract('style','css!sass?sourceMap')
      },
      {test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/font-woff&name=/assets/fonts/[name].[ext]"},
      {test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/font-woff&name=/assets/fonts/[name].[ext]"},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,  loader: "url?limit=10000&mimetype=application/octet-stream&name=/assets/fonts/[name].[ext]"},
      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,  loader: "file?name=/assets/fonts/[name].[ext]"},
      {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,  loader: "url?limit=10000&mimetype=image/svg+xml&name=assets/images/[name].[ext]"}
    ]
  },
  plugins: [
    new ExtractTextPlugin('assets/styles/[name].css'),
    new LiveReloadPlugin({
      port: 35729
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery",
      "Hammer": "hammerjs/hammer"
    })
  ]
};
