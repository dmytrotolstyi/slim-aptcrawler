export default class Pagination {

  constructor(pageSize = 10, pagesCount = 1, currentPage = 1, apiUrl = '') {
    this.pageSize = pageSize;
    this.pagesCount = pagesCount;
    this.currentPage = currentPage;
    this.apiUrl = apiUrl;
    
    this._bindListeners();
  }
  
  _bindListeners() {
    $('.pagination .previous-page').on('click', () => {
      this.getPreviousPage()
    })
    
    $('.pagination .next-page').on('click', () => {
      this.getNextPage()
    })
    
    $('.pagination .page').on('click', (e) => {
      const page = $(e.currentTarget).data('page')
      this._getPage(page)
  })
    
    
  }

  /**
   * Proceeds pagination to next page
   */
  getNextPage() {
    if (this.currentPage < this.pagesCount) {
      return this._getPage(this.currentPage++);
    }
  }

  /**
   * Proceeds pagination to previous page
   */
  getPreviousPage() {
    if (this.currentPage > 1) {
      return this._getPage(this.currentPage--);
    }
  }

  _getPage(page) {
    return $.get(this.apiUrl + '?page=' + page + '&count=' + this.pageSize);
  }
}